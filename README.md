## Installer Sync
Find and download the most recent software installer at the given web address.

Address(es) can be given as command arguments or in an input file.

Install directly from master branch:
```shell
python3 -m pip install pipx # Ensure pipx is installed for better package management
pipx install https://gitlab.com/n8marti/installer-sync/-/archive/master/installer-sync-master.zip
```