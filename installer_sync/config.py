from requests.exceptions import ConnectionError
from requests.exceptions import Timeout
from socket import timeout


DATE_FMT = '%Y-%m-%d %H:%M:%S'
EXTENSIONS = ['exe', 'msp', 'msi', 'zip']
HTTP_TIMEOUT = 30
HTTP_ERRORS = (
    Timeout,
    ConnectionError,
    timeout,
    Exception,
)
MSG_FMT = '%(asctime)s %(levelname)s: %(message)s'
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)'
