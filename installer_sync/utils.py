import logging
import re
import requests
from bs4 import BeautifulSoup
from packaging import version
from pathlib import Path
from urllib.parse import urlparse

from . import config


def valid_url(input_text):
    p = urlparse(input_text)
    if not p.netloc:
        return False
    elif p.scheme not in ['http', 'https', 'ftp', 'ftps']:
        return False
    return True


def write_lines_to_file(lines, filepath):
    filep = Path(filepath)
    text = '\n'.join(lines)
    filep.write_text(f"{text}\n")


def get_installers(url):
    logging.info(f"Getting installer links from \"{url}\"...")
    installers = []
    valid_extensions = config.EXTENSIONS

    # Case 1: If "url" has a valid extension, return it immediately.
    for ext in valid_extensions:
        if url.endswith(f".{ext}"):
            return [url]

    # Get "url" response headers.
    rh = get_response(url, rtype='headers')
    if rh is None:
        return None

    if rh.status_code == 404:
        logging.error(f"{rh.status_code} Not found: {rh.url}")
        return None
    elif rh.status_code == 301:
        rd_link = rh.headers.get('Location')
        logging.error(f"{rh.status_code}; Permanently moved to: {rd_link}")
        logging.debug(f"{rh.status_code}; {rh.headers=}")
        # NOTE: Some sites use 301 (perm. redirect) when 302 (temp. redirect)
        # is more appropriate (e.g.
        # https://paratext.org/download/offline-installer). This workaround
        # allows such links to be added anyway if they have the correct file
        # extenson.
        return [rd_link]
        # # TODO: Some sites redirect to an API link, such as Avast. The URL will  # noqa: E501
        # # download the file, but the filename isn't part of the link. Should
        # # all 301s be treated just like 302s?
        # for ext in valid_extensions:
        #     if rd_link.endswith(f".{ext}"):
        #         return [rd_link]
        # return None

    # Case 2: If "url" redirects to a file, return the redirect link
    # immediately.
    elif rh.status_code == 302:
        rd_link = rh.headers.get('Location')
        logging.info(f"Adding redirect link: {rd_link}")
        return [rd_link]

    elif rh.status_code != 200:
        logging.error(f"{rh.status_code}; {rh.headers}")
        logging.debug(f"{rh.__dict__=}")

    # Case 3: Assume "url" is source page of actual installer(s) & parse it.
    # Filter out link(s) from page.
    r = get_response(url)
    if not r:
        logging.error(f"Could not connect to {url}")
        return None
    soup = BeautifulSoup(r.content, 'html.parser')
    all_links = soup.select('a')
    for a in all_links:
        link = a.get('href')
        logging.debug(f"Checking 'a' href: {link}")
        if link.startswith('//'):  # assume https; fill in schema
            logging.info("Adding 'https:' schema to link.")
            link = f"https:{link}"
        linkp = Path(link)
        if not valid_url(link) and len(linkp.parents) > 1:
            logging.debug(f"Invalid URL: {link}")
            continue
        if linkp.suffix.lstrip('.') in valid_extensions:
            # link = str(linkp)
            if len(linkp.parents) == 1:  # filename only
                link = f"{url.rstrip('/')}/{link}"
            logging.debug(f"Found installer: {link}")
            installers.append(link)
    installers = list(set(installers))
    installers.sort()
    return installers


def get_response(url, headers=None, rtype='full'):
    if not headers:
        headers = {}
    headers['User-Agent'] = config.USER_AGENT
    timeout = config.HTTP_TIMEOUT
    try:
        if rtype == 'full':
            response = requests.get(url, headers=headers, timeout=timeout)
        elif rtype == 'headers':
            response = requests.head(url, headers=headers, timeout=timeout)
        logging.debug(f"{response.status_code=}")
    except config.HTTP_ERRORS as e:
        logging.error(f"{type(e)}: {e}")
        return
    return response


def get_newest_versions(installers):
    """Return the highest version of each installer.

    Assume multiple installers, each with multiple versions.
    """
    newest_installers = []

    installers = list(set(installers))
    installers.sort()
    logging.debug(f"Installers: {', '.join(installers)}")
    unique_installers = {}
    for inst in installers:
        m_ver = re.search(r'[0-9]+\.[0-9]+\.[0-9]+', inst)
        if m_ver:
            ver = m_ver.group()
            name = inst.replace(ver, '')
            if not unique_installers.get(name):
                unique_installers[name] = {ver: inst}
            else:
                unique_installers[name][ver] = inst
        else:
            newest_installers.append(inst)

    # logging.debug(f"{unique_installers=}")
    for name, vers in unique_installers.items():
        versions = [version.parse(k) for k in vers.keys()]
        versions.sort()
        newest_version = str(versions[-1])
        newest_link = vers.get(newest_version)
        logging.debug(f"Newest: {newest_link}")
        newest_installers.append(newest_link)

    return newest_installers


def get_lines(filepath, links=False):
    ltype = 'lines'
    if links:
        ltype = 'links'
    logging.debug(f"Getting valid {ltype} from file: {filepath}")
    lines = []
    infile = Path(filepath)
    if infile.is_file():
        with infile.open() as f:
            for line in f:
                cleanline = line.strip()
                if not cleanline:  # skip blank lines
                    continue
                elif cleanline.startswith('#'):
                    logging.debug(f"Skipping comment: {cleanline}")
                    continue
                elif links and not valid_url(cleanline):
                    logging.error(f"Invalid URL: {cleanline}")
                    continue
                logging.debug(f"Adding line: {cleanline}")
                lines.append(cleanline)
    else:
        logging.error(f"File not found: {infile}")
    return lines


def filter_links(exprs, links):
    filtered = links.copy()
    for expr in exprs:
        if expr.startswith('/') and expr.endswith('/'):  # regex pattern
            # Remove link from list if matching regex 'expr'.
            filter_str = expr.strip('/')
            logging.info(f"Filtering out links containing regex: {filter_str}")
            for link in links:
                if re.search(filter_str, link):
                    try:
                        filtered.remove(link)
                    except ValueError:
                        pass
        else:  # exact string pattern
            # Remove link from list if link contains 'expr'.
            filter_str = expr.lstrip('!')
            logging.info(f"Filtering out links containing: {filter_str}")
            for link in links:
                if filter_str in link:
                    try:
                        filtered.remove(link)
                    except ValueError:
                        pass
    logging.debug(f"{filtered=}")
    return filtered
