import logging
import sys

from . import utils


def get_all_links(links_file, cli_links):
    # Gather valid links from inputs.
    logging.info(f"Getting links from file: {links_file}")
    valid_links = []
    if links_file.is_file():
        valid_links.extend(utils.get_lines(links_file, links=True))
    for link in cli_links:
        if utils.valid_url(link):
            valid_links.append(link)

    if not valid_links:
        logging.critical("No valid input links given.")
        sys.exit(1)

    valid_links = list(set(valid_links))  # deduplicate
    valid_links.sort()
    logging.debug(f"{valid_links=}")
    return valid_links


def get_all_installers(links):
    # Gather installers from valid_links.
    installers = []
    for link in links:
        valid_installers = utils.get_installers(link)
        if valid_installers:
            installers.extend(utils.get_newest_versions(valid_installers))

    if not installers:
        logging.critical("No valid download links found.")
        sys.exit(1)

    logging.debug(f"{installers=}")
    return installers
