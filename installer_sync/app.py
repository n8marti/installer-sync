import argparse
import logging
import net_dl
import sys
import time
from pathlib import Path

from . import __version__
from . import archiver
from . import config
from . import inputs
from . import utils


def download_files(links_file, dest_dir):
    links_file = Path(links_file)
    links = utils.get_lines(links_file, links=True)
    for link in links:
        logging.info(f"Checking link: {link}")
        headers = {'User-Agent': config.USER_AGENT}
        net_dl.Download(
            url=link,
            destdir=dest_dir,
            request_headers=headers
        ).get()


def main():
    parser = argparse.ArgumentParser(
        prog="installer-sync",
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        '-d', '--output-dir', nargs=1,
        help="folder for output files [default: $PWD/installer-sync-files]",
    )
    parser.add_argument(
        '-f', '--input-file', nargs=1,
        help="file with newline-separated list of webpages or download links",
    )
    parser.add_argument(
        '-n', '--no-downloads', action='store_true',
        help="only get links, don't download files",
    )
    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help="give verbose output",
    )
    parser.add_argument(
        '--version', action='version', version=f'%(prog)s {__version__}'
    )
    parser.add_argument(
        'link', metavar='LINK', nargs='*',
        help="space-separated list of webpages or download links",
    )
    parser.add_argument(
        '--debug', action='store_true', help=argparse.SUPPRESS,
    )
    parser.epilog = '''
Special files in OUTPUT_DIR
---------------------------
OUTPUT_DIR/config/input-links.txt
All links passed on the command line will be verified and saved in input-links.txt.

OUTPUT_DIR/config/download-links_TIMESTAMP.txt
All installer links found by evaluating the input links will be saved in download-links_TIMESTAMP.txt.

OUTPUT_DIR/config/filters.txt
If filters.txt is found in the output dir at config/filters.txt, it will be used to filter out links by string or regex matches; e.g.
# This string filter will remove any download links containing the exact string:
win32
# This regex filter will remove any download links containing "win32.exe" or "win32.zip":
/win32\\.(exe|zip)/
'''  # noqa:E501
    args = parser.parse_args()
    timestamp = time.strftime('%Y%m%dT%H%M%S')

    # Set up logging.
    log_level = logging.WARNING
    if args.verbose:
        log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG
    logging.basicConfig(
        level=log_level,
        format=config.MSG_FMT,
        datefmt=config.DATE_FMT,
    )
    logging.debug(f"{args=}")

    # Set outdir.
    outdir = Path().cwd() / 'installer-sync-files'
    if args.output_dir:
        outdir = Path(args.output_dir[0])
    outdir.mkdir(parents=True, exist_ok=True)
    if not outdir.is_dir():
        logging.critical(f"Folder does not exist: {outdir}")
        sys.exit(1)
    logging.debug(f"{outdir=}")

    # Prepare config dir.
    config_dir = outdir / 'config'
    config_dir.mkdir(parents=True, exist_ok=True)
    if not config_dir.is_dir():
        logging.critical(f"Folder does not exist: {config_dir}")
        sys.exit(1)
    logging.debug(f"{config_dir=}")

    # Define input file.
    infile = config_dir / 'input-links.txt'
    if args.input_file:
        infile = Path(args.input_file[0])
        if not infile.is_file():
            logging.critical(f"File does not exist: {infile}")
            sys.exit(1)
    logging.debug(f"{infile=}")

    # Define config files.
    input_links_file = config_dir / "input-links.txt"
    logging.debug(f"{input_links_file=}")
    download_links_file = config_dir / f"download-links_{timestamp}.txt"
    logging.debug(f"{download_links_file=}")

    # Define filter file.
    filter_file = config_dir / "filters.txt"
    # TODO: Generate filter_file with explanatory comments at top of file.
    filter_file.touch()
    logging.debug(f"{filter_file=}")

    # Gather valid links from inputs.
    valid_links = inputs.get_all_links(infile, args.link)

    # Write out input_links file.
    utils.write_lines_to_file(valid_links, input_links_file)

    # Gather installers from valid_links.
    installers = inputs.get_all_installers(valid_links)

    # Filter out unwanted links, if file provided.
    filters = None
    if filter_file.is_file():
        logging.info(f"Getting filters from file: {filter_file}")
        filters = utils.get_lines(filter_file)

    if filters:
        installers = utils.filter_links(filters, installers)

    # Write out download_links file.
    lines = '\n'.join(installers)
    download_links_file.write_text(f"{lines}\n")

    # Download files.
    if not args.no_downloads:
        download_files(download_links_file, outdir)

    # Archive old versions.
    archiver.cleanup_folder(outdir)


def run_cli():
    try:
        main()
    except KeyboardInterrupt:
        print("Cancelled with Ctrl+C")
        sys.exit(1)
