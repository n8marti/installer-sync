"""Keep newest X versions of similarly-named files in a given directory.
(defaults to "2") Older versions get moved to subfolder. (defaults to
"./archive")
"""

import argparse
import logging
import re
import tempfile

from packaging import version
from pathlib import Path


def validate_target_folder(folder_string):
    folder_path = Path(folder_string).resolve()
    if not folder_path.is_dir():
        logging.critical(f"Not a valid folder: {folder_string}")
        exit(1)
    return folder_path


def validate_archive_folder(folder_string):
    # Need ensure that archive folder exists or can be created, and that user
    #   has write permissions to it.
    folder_path = Path(folder_string).resolve()
    # See if it exists or can be created.
    try:
        folder_path.mkdir(parents=True, exist_ok=True)
    except Exception:
        logging.critical(f"Can't create \"{folder_string}\"")
        exit(1)
    # See if user can write to it.
    try:
        with tempfile.TemporaryFile(dir=folder_path, mode='w+t') as tf:
            tf.write('test')
    except PermissionError:
        logging.critical(f"Can't copy files to \"{folder_string}\"")
        try:
            folder_path.rmdir()  # fails if not empty
        except Exception:
            pass
        exit(1)
    return folder_path


def get_version_string(filestem):
    # Examples of filestems:
    #   - Skype-8.74.0.152
    #   - syncthing-windows-amd64-v1.16.1
    #   - linux-generic-hwe-20.04_5.11.0.43.47~20.04.21_amd64
    # The separator is the first character that's not part of the file name.
    # It could be: <space>, <period>, <underscore>, or <dash>
    # Find 'm' where the initial char is followed by valid version chars and
    # ending with 0-9.
    sep_chars = r'[ _.-]{1}'
    version_pat = rf'({sep_chars}[v]*\d+(?:(?:\.\d+)+))'
    m = re.findall(version_pat, filestem, flags=re.IGNORECASE)
    # Get first item in results list.
    version_string = m[0] if m else None
    return version_string


def get_full_filename(filestem, version_string):
    return filestem.replace(version_string, '') if version_string else None


def parse_filename(filepath):
    version_string = get_version_string(filepath.stem)
    # Remove leading character from version_string.
    ver = version.parse(version_string[1:]) if version_string else None
    name = get_full_filename(filepath.stem, version_string)
    return name, ver


def parse_file_list(file_list):
    filetypes = [
        '.deb',
        '.exe',
        '.msi',
        '.msp',
        '.zip',
    ]
    file_data = {}
    for filepath in file_list:
        # Skip folders and unsupported filetypes.
        if filepath.is_dir() or filepath.suffix not in filetypes:
            continue
        name, ver = parse_filename(filepath)
        if not ver:
            # Skip filepath: no recognizable version info.
            logging.info(f"Unknown version; skipping: {filepath}")
            continue
        if not file_data.get(name):
            file_data[name] = [ver]
        else:
            file_data[name].append(ver)
    return file_data


def sort_versions(file_data):
    sorted_data = file_data.copy()
    for name, vers in sorted_data.items():
        sorted_vers = []
        for ver in vers:
            sorted_vers = insert_item(ver, sorted_vers)
        sorted_data[name] = sorted_vers
    return sorted_data


def insert_item(item, items):
    # Insert item into version-sorted location within list of items.
    new_items = items.copy()
    if new_items:
        try:
            new_items.remove(item)
        except ValueError:
            pass
        for i, v in enumerate(items):
            if item > v:
                new_items.insert(i, item)
                break
        if item not in new_items:
            new_items.append(item)
    else:
        new_items.append(item)
    return new_items


def trim_file_list(to_keep, file_data):
    # Trim list of versions to keep.
    trimmed_data = file_data.copy()
    for k, v in trimmed_data.items():
        trimmed_data[k] = v[:to_keep]
    return trimmed_data


def setup_args():
    desc = (
        "Keep N newest versions (default=2) of software package files; "
        "move others to archive folder (default=TARGET_DIR/archive)."
    )
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "target_dir", metavar='TARGET_DIR', type=str, nargs=1,
        help="target folder containing software packages"
    )
    parser.add_argument(
        "-i", "--ask", action='store_true',
        help="ask for confirmation before moving each file"
        )
    parser.add_argument(
        "-d", "--dest", type=str, nargs=1,
        metavar='DEST', default="DEFAULT",
        help="destination folder for removed package files"
        )
    parser.add_argument(
        "-k", "--keep", type=int, metavar='N', default=2,
        help="number of versions of each package to keep"
    )
    return parser.parse_args()


def move_old_files(file_list, kept_data, dest, ask=False):
    dest.mkdir(exist_ok=True)  # ensure dest exists
    for fi in file_list:
        name, ver = parse_filename(fi)
        if kept_data.get(name) and ver not in kept_data.get(name):
            if ask:
                ans = input(f"Move \"{fi.name}\" to {dest}? [Y/n]: ")
                if ans and ans.lower() != 'y':
                    continue
            fi.replace(dest / fi.name)


def cleanup_folder(directorypath, archive=None, ask=False, keep=2):
    directory = Path(directorypath)
    if not directory.is_dir():
        return None

    if not archive:
        archive = directory / 'archive'
    archive.mkdir(exist_ok=True)

    # List files in given directory.
    files = [f for f in directory.iterdir()]
    # Parse file list into dictionary.
    full_data = parse_file_list(files)
    # Sort version numbers highest to lowest.
    sorted_data = sort_versions(full_data)
    # Remove oldest versions from dictionary.
    kept_data = trim_file_list(keep, sorted_data)
    # Move unkept files to archive folder.
    move_old_files(files, kept_data, archive, ask=ask)


def main():
    # Set global variables.
    args = setup_args()
    # Set folder where installer files will be compared and culled.
    target_dir = validate_target_folder(args.target_dir[0])
    # Set archive folder for culled files.
    if args.dest == 'DEFAULT':
        archive = target_dir / 'archive'
    else:
        archive = validate_archive_folder(args.dest[0])
    # Set number of package versions of each file to keep.
    keep = args.keep if args.keep else 2
    # Set confirmation variable.
    ask = args.ask

    # Run program.
    cleanup_folder(target_dir, archive=archive, ask=ask, keep=keep)


if __name__ == '__main__':
    main()
