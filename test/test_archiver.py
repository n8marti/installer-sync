import unittest
from packaging import version
from pathlib import Path

from installer_sync import archiver


class TestArchiver(unittest.TestCase):
    def setUp(self):
        self.fnames = {
            'Firefox Setup 113.0.2.exe': [
                'Firefox Setup.exe',
                ' 113.0.2',
                version.parse('113.0.2'),
            ],
            'LibreOffice_7.4.7_Win_x64_helppack_fr.msi': [
                'LibreOffice_Win_x64_helppack_fr.msi',
                '_7.4.7',
                version.parse('7.4.7'),
            ],
            'mb4-setup-consumer-4.5.29.268-1.0.2022-1.1.69620.exe': [
                'mb4-setup-consumer-1.0.2022-1.1.69620.exe',
                '-4.5.29.268',
                version.parse('4.5.29.268'),
            ],
            'Paratext_9.4.101.1_InstallerOffline.exe': [
                'Paratext_InstallerOffline.exe',
                '_9.4.101.1',
                version.parse('9.4.101.1'),
            ],
            'Skype-8.74.0.152.exe': [
                'Skype.exe',
                '-8.74.0.152',
                version.parse('8.74.0.152'),
            ],
            'syncthing-windows-amd64-v1.16.1.zip': [
                'syncthing-windows-amd64.zip',
                '-v1.16.1',
                version.parse('1.16.1'),
            ],
        }

    def test_get_full_filename(self):
        for fname, parts in self.fnames.items():
            ffn = archiver.get_full_filename(fname, parts[1])
            self.assertEqual(ffn, parts[0])

    def test_get_version_string(self):
        for fname, parts in self.fnames.items():
            v = archiver.get_version_string(fname)
            self.assertEqual(v, parts[1])

    def test_parse_filename(self):
        for fname, parts in self.fnames.items():
            name, ver = archiver.parse_filename(Path(fname))
            self.assertEqual(ver, parts[2])
